"""
Load a configuration file written in YAML format.
"""

import yaml

def read_config(file_name):

    with open(file_name, "r") as ymlfile:
        config = yaml.load(ymlfile)

    return config


config = read_config("test_config.yml")

for key, value in config.items():
    if isinstance(value, dict):
        print(key,":")
        for key2, value2 in value.items():
            if isinstance(value2, dict):
                print("  ", key2, ":")
                for key3, value3 in value2.items():
                    print("    ", key3, value3)
            else:
                print("  ", key2, value2)
    else:
        print(key, value)

print(type(config["part3"]["key1"]))
for item in config["part3"]["key1"]:
    print(item)
