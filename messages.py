# Copyright (C) 2016  Tobias Gronemeier, Lennart Nils Böske
#
#
# TOPAS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
Terminal color messages
"""

class tc:
    NORM = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    #normal colours
    BLACK   = '\033[30m'
    RED     = '\033[31m'
    GREEN   = '\033[32m'
    YELLOW  = '\033[33m'
    BLUE    = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN    = '\033[36m'
    GREY    = '\033[37m'

    #intense colours
    INTBLACK   = '\033[90m'
    INTRED     = '\033[91m'
    INTGREEN   = '\033[92m'
    INTYELLOW  = '\033[93m'
    INTBLUE    = '\033[94m'
    INTMAGENTA = '\033[95m'
    INTCYAN    = '\033[96m'
    INTGREY    = '\033[97m'

#-------------------------------------------------------------------------------
#                   Informative terminal messages with colors
#-------------------------------------------------------------------------------
def change_color():
    print(tc.GREY+'\033[A')

def change_normal():
    print(tc.NORM)

def message(string):
    print("")
    print( tc.YELLOW + "* " + string + tc.NORM )

def message_info(string):
    print( tc.BLUE + "   info: " + string + tc.NORM )

def message_done():
    print( tc.GREEN + "   --> Done" + tc.NORM )

def message_warning(string):
    print( tc.RED + "   warning: " + string + tc.NORM )

def message_error(string):
    print("")
    print( tc.RED + tc.BOLD + "## ERROR: " + string + tc.NORM )

def message_ok(string):
    print("")
    print(tc.GREEN + "* " + string + tc.NORM )

def message_start(string):
    print(".-----------------------------------------------------------.")
    print("| "+tc.BOLD+tc.BLUE+string+tc.NORM+" |")
    print("| Copyright (C) 2019  T. Gronemeier                         |")
    print("|-----------------------------------------------------------|")
    print("| This program comes with ABSOLUTELY NO WARRANTY. This is   |")
    print("| free software, and you are welcome to redistribute it     |")
    print("| under certain conditions; see 'COPYING' for details.      |")
    print("'-----------------------------------------------------------'")
