# List of additional functions and routines

from netCDF4 import Dataset
import numpy as np
from messages import *


def find_nearest(array, values):
    """Find indices of element in array nearest to values.
    If multiple elements are equally close to values,
    all indices are returned."""
    indices = []
    array = np.asarray(array)
    for val in values:
        # indices.append((np.abs(array - val)).argmin())
        diff = np.abs(array - val)
        indices.append(np.where(diff==diff.min()))
    return indices


def get_index(dim, values):
    """Find indices of dim where dim is closest to values.
    dim must be a 1D numpy.array and monotonically increasing;
    the returned indices are a list."""
    indices = []
    for val in values:
        if val < min(dim):
            index = 0
        elif val > max(dim):
            index = len(dim) - 1
        else:
            index = min(np.where(dim<=val)[0][-1],
                    np.where(dim>=val)[0][0])
            try:
                if abs(dim[index-1]-val) < abs(dim[index]-val):
                    index-=1
                if abs(dim[index+1]-val) < abs(dim[index]-val):
                    index+=1
            except:
                index=index
        indices.append(index)
    return indices


def open_netcdf(filename):
    """Read NetCDF data and return file as a netCDF4.Dataset"""
    message_info("read netCDF file")

    try:
        f = open(filename)
        f.close()
    except FileNotFoundError:
        message_error("Error: " + filename + ". No such file. Aborting...")
        sys.exit(1)

    nc_file = Dataset(filename, "r+", format="NETCDF4")

    message_done()

    return nc_file


def read_numpy_array_from_txt(file):
    """Read a numpy array from file"""
    message_info("read numpy array")

    data = np.genfromtxt(file)

    message_done()
    return data
