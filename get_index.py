#!/usr/bin/python3
import numpy as np

def get_index(dim, values):
    """Find index of dim where dim is closest to specified values."""
    positions=[]
    for val in values:
        if val < min(dim):
            pos = 0
        elif val > max(dim):
            pos = len(dim) - 1
        else:
            pos = min(np.where(dim<=val)[0][-1],
                    np.where(dim>=val)[0][0])
            try:
                if abs(dim[pos-1]-val) < abs(dim[pos]-val):
                    pos-=1
                if abs(dim[pos+1]-val) < abs(dim[pos]-val):
                    pos+=1
            except:
                pos=pos
        positions.append(pos)
    return positions

#Define test dimension variable
dim = np.arange(10)*2.+1.0
print('Testdimension:', dim)

#Do some tests
val=[5.0, 5.2, 6.8, 6.0, -10.0, 100.0]

print('gesucht:\n', val, '\n gefunden:\n', dim[get_index(dim, val)])
